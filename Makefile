build:  *.hs
	ghc --make -rtsopts Main
prof:  *.hs
	ghc --make -prof -rtsopts Main
	
clean:
	rm *.hi *.o Main *.prof *.ps *.aux *.hp


