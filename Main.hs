{-# LANGUAGE BangPatterns #-}
import Prelude
import qualified Data.List(foldl') as L
import System(getArgs)
import qualified Data.ByteString.Lex.Double as D
import qualified Control.Monad as CM
import qualified Data.CSV.Enumerator as E
import GHC.Exception
import qualified Data.ByteString.Char8 as B
import Coords

settings = E.defCSVSettings 
    { E.csvSep = ','
    , E.csvQuoteChar = Nothing
    }

-- Apply the accumulator to each row of the dataFile and return the result
cityFold :: String -> (a -> E.ParsedRow E.Row -> a) -> a -> IO (Either SomeException a)
cityFold dataFile foldF startVal = E.foldCSVFile dataFile settings (E.funToIter foldF) startVal


countRow::Int -> E.ParsedRow E.Row -> Int
countRow acc _ = acc + 1

cityIO acc startval onRight dataFile = do
    result <- cityFold dataFile acc startval
    case result of
        Left e -> putStrLn $ "Whoopsie: " ++ (show e)
        Right r -> onRight r

countCities = cityIO countRow 0 (\t -> putStrLn $ "Got: " ++ (show t) ++ " rows")

data City = City 
    { country :: B.ByteString
    , safeName :: B.ByteString
    , name :: B.ByteString
    , region :: B.ByteString 
    , population :: Maybe Int
    , lat :: Double
    , long :: Double
    } deriving (Show)

parseFirst :: [City] -> E.ParsedRow E.Row -> [City]
parseFirst prev row =
    case prev of
        [] -> simpleParser prev row
        _ -> prev
            
simpleParser :: [City] -> E.ParsedRow E.Row -> [City]
simpleParser !prev !row =
    case row of
        E.EOF -> prev
        E.ParsedRow Nothing -> prev
        E.ParsedRow (Just r) -> case (parseFields r) of
            Nothing -> prev
            (Just c) -> c:prev

type BinnedCities = [(Bin, Int)]
emptyBins = map (flip (,) $ 0) (geoGrid 10)

{-
binningParser   :: State BinnedCities  [Bin]
    -> E.ParsedRow E.Row 
    -> State BinnedCities [Bin]
    -}
binningParser prev@(bins, !count) row =
    case row of
    E.ParsedRow (Just r) -> case (parseFields r) of
        Nothing -> prev
        (Just c) -> (bins, (update count (findBin bins (lat c, long c))))
            where
                update old b = L.foldl' (\acc -> \(bin, c) -> if (bin == b) then (bin, c + 1):acc else (bin, c):acc) [] old
    otherwise -> prev
parseFields :: [E.Field] -> Maybe City
parseFields cs = 
    if (length cs /= 7) then Nothing
    else do
        (lat, _) <- D.readDouble (cs!!5)
        (long, _) <- D.readDouble (cs!!6)
        pop <- if (B.length (cs!!4) == 0)
                    then Just Nothing
                    else case (B.readInt $ cs!! 4) of
                        Nothing -> Nothing
                        Just (val, _) -> Just $ Just val
        return City 
            { country = cs!!0
            , safeName = cs!!1
            , name = cs!!2
            , region = cs!!3
            , population = pop
            , lat = lat
            , long = long
            }

printFirst cities = putStrLn . B.unpack . safeName $ cities!!0
firstCityName = cityIO parseFirst [] printFirst
parseAll = cityIO simpleParser [] printFirst
binAll = cityIO 
        binningParser 
        (geoGrid 3, map (flip (,) $ 0) $ geoGrid 3)
        (\bins -> CM.forM_ (map show $ snd bins) putStrLn)
main = do 
    args <- getArgs
    case args of
        cmd:path:[] -> case cmd of
            "f" -> firstCityName path
            "a" -> parseAll path
            "b" -> binAll path
            "c" -> countCities path
            otherwise -> putStrLn $ "Unknown command " ++ cmd
        otherwise -> putStrLn "Requires exactly two arguments: the action to perform (single letter) and the file to process."

