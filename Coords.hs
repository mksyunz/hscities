module Coords (grid, r, geoGrid, findBin, Coord, Bin) where

r x0 x1 n | x0 < x1 && n >= 1  = Left $ [x0, x0 + d .. (x1 - d)] ++ [x1] where d = (x1 - x0)/n
r x0 x1 n | otherwise = Right $ "Invalid arguments to r: " ++ (show x0) ++ ", " ++ (show x1) ++ ", " ++ (show n)

lambdaR n = case r (-180) 180 n of 
    Left r -> r 
    Right _ -> undefined

phiR n = case r (-90) 90 n of
    Left r -> r
    Right _ -> undefined

type Coord = (Double, Double)
type Bin = ((Double, Double), (Double, Double)) 

grid :: [Double] -> [Double] -> [Bin]
grid p1 p2
    = [((x0, y0), (x1, y1)) | (x0, x1) <- pairs p1, (y0, y1) <- pairs p2]
        where pairs xs = zip xs (drop 1 xs)

geoGrid n = grid (phiR n) (lambdaR n)

findBin :: [Bin] -> Coord -> Bin
findBin bins (lat, long) = 
    let checkIt found bin@((x0, y0), (x1, y1)) = 
            if (x0 < lat && lat <= x1 && y0 < long && long <= y1) 
                then bin:found
                else found
    in case (foldl checkIt [] bins) of
        [b] -> b
        otherwise -> error $ "No bin for " ++ (show (lat, long))
